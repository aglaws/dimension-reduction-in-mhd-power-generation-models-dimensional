function [X, f, df] = hartmann_UavgLOG(X)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   Inputs:
%          X: M-by-5 array of inputs where inputs are ordered as follows:
%                    [log(mu) log(rho) log(dpdx) log(eta) log(B0)]
%
%  Outputs:
%          f: m-by-1 array of function evaluations
%          df: M-by-m array of gradient evaluations
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set lower and upper bounds for inputs
lb = log([0.05 1 0.5 0.5 0.1]); ub = log([0.2 5 3 3 1]);

% Transform inputs from [-1,1] hypercube to physical (non-log) ranges
X = exp(bsxfun(@plus,bsxfun(@times,0.5*(X + 1),ub-lb),lb));

M = size(X, 1);

mu = X(:, 1);
rho = X(:, 2);
dpdx = X(:, 3);
eta = X(:, 4);
B0 = X(:, 5);

% Evaluate average flow velocity with respect to physical inputs
f = -dpdx.*(eta./B0.^2).*(1 - B0./sqrt(eta.*mu).*coth(B0./sqrt(eta.*mu)));

% Evaluate gradient with respect to physical inputs
df_dmu = -dpdx./(2*B0.*mu.^2).*(sqrt(eta.*mu).*coth(B0./sqrt(eta.*mu)) - B0.*csch(B0./sqrt(eta.*mu)).^2);
df_drho = zeros(M,1);
df_ddpdx = -(eta./B0.^2).*(1 - B0./sqrt(eta.*mu).*coth(B0./sqrt(eta.*mu)));
df_deta = -dpdx./(2*eta.*mu.*B0.^2).*(2*eta.*mu - B0.*(sqrt(eta.*mu).*coth(B0./sqrt(eta.*mu)) + B0.*csch(B0./sqrt(eta.*mu)).^2));
df_dB0 = -dpdx./(mu.*B0.^3).*(-2*eta.*mu + B0.*(sqrt(eta.*mu).*coth(B0./sqrt(eta.*mu)) + B0.*csch(B0./sqrt(eta.*mu)).^2));

df = [df_dmu, df_drho, df_ddpdx, df_deta, df_dB0];

% Apply log-transform to gradient
df = bsxfun(@times, 0.5*(ub-lb), bsxfun(@rdivide, X.*df, f));

% Transform physical inputs to log-transformed inputs
X = 2*bsxfun(@rdivide, bsxfun(@minus, log(X), lb), ub-lb) - 1;

end