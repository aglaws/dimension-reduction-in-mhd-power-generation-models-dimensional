function problems_tests

addpath('../demo');
try
    
    %% twobytwo
    P = twobytwo_func(0.01); % a harder problem
    X = pseudospectral(P.solve,P.s,10);
    residual_error_estimate(X,P.Av,P.b)
    % eek, bad accuracy, use 100 points!
    X = pseudospectral(P.solve,P.s,100);
    residual_error_estimate(X,P.Av,P.b)
    
    
catch me
    rmpath('../demo');
    rethrow(me);
end
rmpath('../demo');
