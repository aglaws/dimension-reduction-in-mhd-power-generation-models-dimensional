clear all
close all
clc

addpath 'Subspaces'

% Set quantity of interest
% qoi_flag = 1   -->   B_ind
% qoi_flag = 2   -->   U_avg
qoi_flag = 1; 

% Import data file for MHD generator problem 
if qoi_flag == 1 % output = total induced magnetic field
    data = importdata('MHD_Generator_Data_Bind.txt');
    qoi_label = 'gen_bind';
elseif qoi_flag == 2 % output = average flow velocity
    data = importdata('MHD_Generator_Data_Uavg.txt');
    qoi_label = 'gen_uavg';
end 

% Remove failed/unstable runs
index = [6, 41, 73, 108, 116, 154, 254, 259, 296, ...
         306, 310, 348, 399, 412, 454, 471, 468];
data(index, :) = [];

% Get inputs, outputs, and gradients from data
m = 5; M = size(data, 1);
X = data(:, 1:m); f = data(:, m+1); df = data(:, m+2:end);

% Set lower and upper bounds for inputs
lb = log([1e-3, 1e-1, 1e-1, 1e-1, 1e-1]); ub = log([1e-2, 1e1, 5e-1, 1e1, 1e0]);

% Apply log-transform to gradient
df = bsxfun(@times, 0.5*(ub-lb), bsxfun(@rdivide, X.*df, f));

% Transform physical inputs to log-transformed inputs
X = 2*bsxfun(@rdivide, bsxfun(@minus, log(X), lb), ub-lb) - 1;

% Perform active subspace analysis
sub = compute(X, f, df, [], 0, 0, 500);

n = 2;
sub.W1 = sub.eigenvectors(:, 1:n);
sub.W2 = sub.eigenvectors(:, n+1:end);

% Plot eigenvalues with bootstrap errors
close all;
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
semilogy(1:m, sub.eigenvalues, 'ko-', ...
    1:m, sub.e_br(:,1),'k--', ...
    1:m, sub.e_br(:,2),'k--', ...
    'LineWidth', 2, 'MarkerSize', 12);
axis square; grid on;
set(gca, 'FontSize', 14,'XLim', [1, m], 'XTick', 1:m);
xlabel('Index'); ylabel('Eigenvalues');
if qoi_flag == 1
    ylim([1e-10 1e2]);
    set(gca, 'YTick', [1e-10, 1e-8, 1e-6, 1e-4, 1e-2, 1e0, 1e2])
elseif qoi_flag == 2
    ylim([1e-7 1e2]);
    set(gca, 'YTick', [1e-6, 1e-4, 1e-2, 1e0, 1e2])
end
legend('evals','bootstrap','Location','SouthWest');
print(sprintf('figs/%s_evals',qoi_label),'-depsc2');

% Plot first two eigenvectors
figure(2);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
plot(1:m, sub.eigenvectors(:,1), 'ko-', ...
    1:m, sub.eigenvectors(:,2), 'kx--', ...
    'LineWidth', 2, 'MarkerSize', 12);
axis square; grid on;
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off', 'XLim', [1, m], 'XTick', 1:m);
ylim([-1 1]);
legend('evec 1','evec 2','Location','NorthWest');
xlabel('Index'); ylabel('Eigenvector components');
print(sprintf('figs/%s_evecs',qoi_label),'-depsc2');

% Plot 1D summary plot of data
figure(3);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
plot(X*sub.eigenvectors(:,1), f, 'ko', ...
    'LineWidth', 2, 'MarkerSize', 10, 'MarkerFaceColor','k');
axis square; grid on;
xlim([-1.5 1.5]);
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off');
xlabel('$\mathbf{w}_1^T\mathbf{x}$','interpreter','latex'); 
ylabel('$f(\mathbf{x})$','interpreter','latex');
print(sprintf('figs/%s_ssp1',qoi_label),'-depsc2');

% Plot 2D summary plot of data
figure(4);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
scatter(X*sub.eigenvectors(:,1), X*sub.eigenvectors(:,2), ...
    80, f, 'filled');
axis square; grid on;
xlim([-1.5 1.5]); ylim([-1.5 1.5]);
colorbar;
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off', ...
    'XTick', -2:2, 'YTick', -2:2);
xlabel('$\mathbf{w}_1^T\mathbf{x}$','interpreter','latex'); 
ylabel('$\mathbf{w}_2^T\mathbf{x}$','interpreter','latex');
print(sprintf('figs/%s_ssp2',qoi_label),'-depsc2');