function [X, f, df] = hartmann_BindLOG(X)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   Inputs:
%          X: M-by-5 array of inputs where inputs are ordered as follows:
%                    [log(mu) log(rho) log(dpdx) log(eta) log(B0)]
%
%  Outputs:
%          f: m-by-1 array of function evaluations
%          df: M-by-m array of gradient evaluations
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set lower and upper bounds for inputs
lb = log([0.05 1 0.5 0.5 0.1]); ub = log([0.2 5 3 3 1]);
X = exp(bsxfun(@plus,bsxfun(@times,0.5*(X + 1),ub-lb),lb));

M = size(X, 1);
    
mu = X(:, 1);
rho = X(:, 2);
dpdx = X(:, 3);
eta = X(:, 4);
B0 = X(:, 5);
   	
% Evaluate total induced magnetic field with respect to physical inputs
f = dpdx./(2*B0.^2).*(B0 - 2*sqrt(eta.*mu).*tanh(B0./(2*sqrt(eta.*mu))));

% Evaluate gradient with respect to physical inputs
df_dmu = dpdx./(4*mu.*B0.^2).*sech(B0./(2*sqrt(eta.*mu))).^2.*(B0 - sqrt(eta.*mu).*sinh(B0./sqrt(eta.*mu)));
df_drho = zeros(M,1);
df_ddpdx = (B0 - 2*sqrt(eta.*mu).*tanh(B0./(2*sqrt(eta.*mu))))./(2*B0.^2);
df_deta = dpdx./(4*eta.*B0.^2).*sech(B0./(2*sqrt(eta.*mu))).^2.*(B0 - sqrt(eta.*mu).*sinh(B0./sqrt(eta.*mu)));
df_dB0 = dpdx./(2*B0.^3).*(-B0 - B0.*sech(B0./(2*sqrt(eta.*mu))).^2 + 4*sqrt(eta.*mu).*tanh(B0./(2*sqrt(eta.*mu))));

df = [df_dmu, df_drho, df_ddpdx, df_deta, df_dB0];

% Apply log-transform to gradient
df = bsxfun(@times, 0.5*(ub-lb), bsxfun(@rdivide, X.*df, f));

% Transform physical inputs to log-transformed inputs
X = 2*bsxfun(@rdivide, bsxfun(@minus, log(X), lb), ub-lb) - 1;

end