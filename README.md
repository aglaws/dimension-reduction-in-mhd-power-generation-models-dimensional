Scripts for generating figures for the paper

'Dimension reduction in MHD power generation models: dimensional analysis and active subspaces'
A. Glaws, P. G. Constantine, J. Shadid, and T. M. Wildey

https://arxiv.org/abs/1609.01255