clear all
close all
clc

addpath 'pmpack' 'Subspaces'

% Set quantity of interest
% qoi_flag = 1   -->   B_ind
% qoi_flag = 2   -->   U_avg
qoi_flag = 1; 

% Get input values according to Gaussian quadrature rule
m = 5; s = [];
for i=1:m
    s = [s; parameter()];
end
[X, nu] = gaussian_quadrature(s, 11*ones(1, m));

M = size(X,1);

% Evaluate Hartmann problem at inputs 
if qoi_flag == 1 % output = total induced magnetic field
    [X, f, df] = hartmann_BindLOG(X);
    qoi_label = 'hartmann_bind';
elseif qoi_flag == 2 % output = average flow velocity
    [X, f, df] = hartmann_UavgLOG(X);
    qoi_label = 'hartmann_uavg';
end

% Perform active subspace analysis
sub = compute(X, f, df, nu, 0, 0, 500);

n = 2;
sub.W1 = sub.eigenvectors(:, 1:n);
sub.W2 = sub.eigenvectors(:, n+1:end);

% Plot eigenvalues with bootstrap errors
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
semilogy(1:m, sub.eigenvalues, 'ko-', ...
    1:m, sub.e_br(:,1),'k--', ...
    1:m, sub.e_br(:,2),'k--', ...
    'LineWidth', 2, 'MarkerSize', 12);
axis square; grid on;
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off', 'XLim', [1, m], ...
    'XTick', 1:m, 'YTick', 10.^(-15:5:0));
ylim([1e-16 1e1]);
xlabel('Index'); ylabel('Eigenvalues');
legend('evals','bootstrap','Location','NorthEast');
print(sprintf('figs/%s_evals',qoi_label),'-depsc2');

% Plot first two eigenvectors
figure(2);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
plot(1:m, sub.eigenvectors(:,1), 'ko-', ...
    1:m, sub.eigenvectors(:,2), 'kx--', ...
    'LineWidth', 2, 'MarkerSize', 12);
axis square; grid on;
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off', 'XLim', [1, m], 'XTick', 1:m);
ylim([-1 1]);
legend('evec 1','evec 2','Location','NorthWest');
xlabel('Index'); ylabel('Eigenvector components');
print(sprintf('figs/%s_evecs',qoi_label),'-depsc2');

% Set a random subset of model evaluations to plots
ind = randi(M,1000,1);

% Plot 1D summary plot of data
figure(3);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
plot(X(ind,:)*sub.eigenvectors(:,1), f(ind), 'ko', ...
    'LineWidth', 2, 'MarkerSize', 10, 'MarkerFaceColor','k');
axis square; grid on;
xlim([-2 2]);
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off');
xlabel('$\mathbf{w}_1^T\mathbf{x}$','interpreter','latex'); 
ylabel('$f(\mathbf{x})$','interpreter','latex');
print(sprintf('figs/%s_ssp1',qoi_label),'-depsc2');

% Plot 2D summary plot of data
figure(4);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);
scatter(X(ind,:)*sub.eigenvectors(:,1), X(ind,:)*sub.eigenvectors(:,2), ...
    80, f(ind), 'filled');
axis square; grid on;
xlim([-2 2]); ylim([-2 2]);
colorbar;
set(gca, 'FontSize', 14, 'YMinorGrid', 'Off', ...
    'XTick', -2:2, 'YTick', -2:2);
xlabel('$\mathbf{w}_1^T\mathbf{x}$','interpreter','latex'); 
ylabel('$\mathbf{w}_2^T\mathbf{x}$','interpreter','latex');
print(sprintf('figs/%s_ssp2',qoi_label),'-depsc2');